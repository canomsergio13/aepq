# 1. Roles de Usuarios

    -User
    -Admin
    -SuperAdmin
    -Observador
    -Resolver

# 2. Registro de Usuarios

    -Nombre de usuario
    -Correo
    -Contraseña
    -Nombre completo
    -Puesto
    -Empresa

# 3. Creación de Tickets

    -Asunto
    -Descripción
    -Archivos
--------------------------
    -Fecha de creacion
    -Tipo
    -Urgencia
    -Prioridad
    -Fecha de estimado de cierre
    -Duracion


# 4. Asignación de Tickets

    -Un usuario puede crear varios tickets
    -Un ticket Puede ser asignado a varios Resolvers
    -Un ticket Puede ser observado por varios Observadores
    -Un ticket Puede ser cerrado por cualquier Resolvers

# 5. Estado de Tickets

    -Abierto
    -En curso
    -Resuelto
    -Cerrado

# 6. Visualización
    
    -Un usuarios puede ver solo sus tickets
    -El admin y SuperAdmin pueden ver todos los Tickets
    -El observador solo puede ver los tickets a los que es observador
    -El resolver solo puede ver los tickets a los que fue asignado

# 7. Priorización

    -Mediante casos

# 8. Búsqueda y Filtros
# 9. Informes y Estadísticas

    -Dashboard
    -Reportes

# 10. Notificaciones

    -Correo

# 11. Comentarios
    -Un ticket puede tener muchas comentarios

# 12. Empresas

    User
        -id          ->  int
        -full Name   ->  varchar
        -email       ->  varchar
        -password    ->  varchar
        -phone       ->  varchar
        -rol_id      ->  int
        -company_id  ->  int

    Rol
        -id          ->  int 
        -name        ->  varchar

    Company
        -id          ->  int
        -name        ->  varchar
        -location    ->  varchar

    Tickets
        -id          ->  int
        -title       ->  varchar
        -description ->  text
        -created_at  ->  date
        -type        ->  varchar
        -urgency     ->  varchar
        -priority    ->  varchar
        -duration    ->  varchar
        -status      ->  varhcar
        -resolve_date->  date
        -category_id ->  int
        -deadline    ->  date

    Category
        id          ->  int
        name        ->  varchar


    Comments
        -id          ->  int
        -ticket_id   ->  int
        -user_id     ->  int
        -comment     ->  text
        -created_at  ->  date


-- Creación de la tabla de Roles
CREATE TABLE Rol (
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    name VARCHAR(255) NOT NULL
);

-- Creación de la tabla de Company
CREATE TABLE Company (
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    name VARCHAR(255) NOT NULL,
    location VARCHAR(255)
);

-- Creación de la tabla de Users
CREATE TABLE User (
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    full_name VARCHAR(255) NOT NULL,
    email VARCHAR(255) UNIQUE NOT NULL,
    password VARCHAR(255) NOT NULL,
    phone VARCHAR(20),
    rol_id INT,
    company_id INT,
    FOREIGN KEY (rol_id) REFERENCES Rol(id),
    FOREIGN KEY (company_id) REFERENCES Company(id)
);

-- Creación de la tabla de Tickets
CREATE TABLE Tickets (
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    title VARCHAR(255) NOT NULL,
    description TEXT,
    created_at DATE,
    type VARCHAR(50),
    urgency VARCHAR(50),
    priority VARCHAR(50),
    duration VARCHAR(50),
    status VARCHAR(50),
    resolve_date DATE,
    deadline DATE,
    category_id INT,
    FOREIGN KEY (category_id) REFERENCES Category(id)
);

-- Creación de la tabla de Comments
CREATE TABLE Comments (
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    ticket_id INT NOT NULL,
    user_id INT NOT NULL,
    comment TEXT NOT NULL,
    created_at DATE,
    FOREIGN KEY (ticket_id) REFERENCES Tickets(id),
    FOREIGN KEY (user_id) REFERENCES User(id)
);

CREATE TABLE Category (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

