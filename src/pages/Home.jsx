import { QRCode } from "antd"
import CardNews from "../components/CardNews"
import CardPrimary from "../components/CardPrimary"
import Carousel from "../components/Carousel"
import MyCard from "../components/MyCard"
import NavBar from "../components/NavBar"
import { Button, Card, CardBody, FormControl, FormLabel, Input, useToast } from '@chakra-ui/react'
import {Modal, ModalContent, ModalHeader, ModalBody, ModalFooter, useDisclosure} from "@nextui-org/react";
import { useState } from "react"
import axios from "axios"


const downloadQRCode = name => {

    const a = document.createElement('a');
    a.download = name;
    a.href = `/${name}`;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    
};

const Home = () => {

    const {isOpen, onOpen, onOpenChange, onClose} = useDisclosure();
    const toast = useToast()

    const [Data, setData] = useState({
        app_id: 5,
        email: "",
        full_name:""
    })

    const images = [
        { id: 1, src: '/b1.jpg', date: '20 de junio de 2023' },
        { id: 2, src: '/b2.jpg', date: '28 de junio de 2023' },
        { id: 3, src: '/b3.jpg', date: '29 de junio de 2023' },
    ]

    const submit = async () => {
        
        if(
            Data.email === "" ||
            Data.full_name === ""
        ) {
            toast({
                title: 'Campos imcompletos',
                description: "El nombre y correo es obligatorio",
                status: "warning",
                duration: 9000,
                isClosable: true,
                position: "top-right"
            })
            return
        }

        onClose()

        try {
            await axios.post("https://victum.eastus.cloudapp.azure.com:3210/orbnote/victum/api/v1/packages/14/app_requests", Data)
        } catch (error) {
            console.log(error)
        }

        toast({
            title: 'Solicitud Creada',
            description: "Su solicitud se ha enviado correctamente",
            status: 'success',
            duration: 9000,
            isClosable: true,
            position: "top-right"
        })

        setData({
            app_id: 7923,
            email: "",
            full_name:""
        })

    } 

    const manual = () => window.open("https://www.canva.com/design/DAFtg2G_VKs/QC8ihgySy-Be72TvNEgrtA/view?website#2","_blank")

    return (
        <>
            <NavBar />
            <Carousel />
            <section className="mx-auto max-w-screen-lg py-5" >
                <section className="map_info flex flex-col items-center lg:flex-row lg:justify-center lg:items-center lg:mt-10 relative top-[-225px]" >
                    <div className="w-full lg:w-1/3 mb-4 lg:mb-0">
                        <CardPrimary />
                    </div>

                    <div className="w-full lg:w-2/3 shadow-lg mx-2">
                        <Card>
                            <CardBody>
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3866.5810359361767!2d-90.78144259090715!3d14.277638665628418!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8588e29ba473b615%3A0x9b68d60e38ef825!2sAutop.%20Puerto%20Quetzal%2C%20Escuintla%2C%20Guatemala!5e0!3m2!1ses!2smx!4v1692371896978!5m2!1ses!2smx" width="100%" height="350" allowFullScreen="" loading="lazy" referrerPolicy="no-referrer-when-downgrade"></iframe>
                            </CardBody>
                        </Card>
                    </div>

                </section>

                <section className="flex flex-col items-center lg:flex-row lg:justify-center lg:items-start mt-[-225px]">
                    {
                        images.map(image =>         
                            <div key={image.id} className="w-full lg:w-1/3 mx-1 px-5 lg:mb-0">
                                <CardNews 
                                    src={image.src} 
                                    date={image.date}
                                />
                            </div>
                        )
                    }
                </section>

                <section className="flex flex-col items-center lg:flex-row lg:justify-center lg:items-start mt-[-10px]">


                    {
                        ['victum-sos.apk'].map(card =>
                        
                            <div className="w-full lg:w-1/3 mx-1 px-5 lg:mb-0" key={card}>
                                <MyCard>
                                    <div id="myqrcode" className="mx-auto flex flex-col justify-center items-center" >
                                        <QRCode
                                            value={`https://aepq.victum-re.online/${card}`}
                                            bgColor="#fff"
                                            style={{ marginBottom: 16}}
                                            size={200}
                                        />
                                        <Button 
                                            colorScheme={card==='victum-sos.apk'?'green' :'blue'}
                                            onClick={()=>downloadQRCode(card)}
                                        >
                                             Descargar App {card==='victum-sos.apk'?' Android' :' iOS'}
                                        </Button>
                                    </div>
                                </MyCard>
                            </div>
                        
                        )
                    }


                    <div className="w-full lg:w-1/3 mx-1 px-5 lg:mb-0"  onClick={onOpen}  style={{ cursor: "pointer" }} >
                        <MyCard>
                            <div id="myqrcode" className="mx-auto flex flex-col justify-center items-center" >
                                <div  style={{ width: "12rem", marginTop: 5, marginBottom: 10 }} >
                                    <img src="/App_Store.png" />
                                </div>
                                <Button 
                                    colorScheme={'blue'}
                                    onClick={onOpen}
                                >
                                        Solicitar App iOS
                                </Button>
                            </div>
                        </MyCard>
                    </div>
                 
                </section>

                <Modal isOpen={isOpen} onOpenChange={onOpenChange}>
                    <ModalContent>
                        {() => (
                            <>
                                <ModalHeader className="flex flex-col gap-1">Solicitud para App</ModalHeader>
                                <ModalBody>
                                    <FormControl>
                                        <FormLabel>Nombre:</FormLabel>
                                        <Input type='text' onChange={(e) => setData({ ...Data, full_name: e.target.value })} />
                                    </FormControl>
                                    <FormControl>
                                        <FormLabel>Correo Electrónico (Apple ID): </FormLabel>
                                        <Input type='email' onChange={(e) => setData({ ...Data, email: e.target.value })} />
                                    </FormControl>
                                </ModalBody>
                                <ModalFooter>
                                    <Button colorScheme='green' onClick={manual}  >
                                        Ver manual
                                    </Button>
                                    <Button colorScheme='blue' onClick={submit}  >
                                        Solicitar App iOS
                                    </Button>
                                </ModalFooter>
                            </>
                        )}
                    </ModalContent>
                </Modal>

            </section>
        </>
    )
}

export default Home