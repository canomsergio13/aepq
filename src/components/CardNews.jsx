import {
  Box,
  Center,
  Text,
  Stack,
  Avatar,
  useColorModeValue,
  Image
} from '@chakra-ui/react'
import PropTypes from 'prop-types'

export default function CardNews({src,date}) {
    return (
        <Center py={6}>
            <Box
                maxW={'400px'}
                w={'full'}
                bg={useColorModeValue('white', 'gray.900')}
                boxShadow={'2xl'}
                rounded={'md'}
                p={6}
                overflow={'hidden'}
            >
                <Box h='100%' bg='gray.100' mt={-6} mx={-6} mb={6} pos={'relative'}>
                    <Image
                        src={src}
                        fill
                        alt="siva"
                        w='100%'
                        overflow={'hidden'}
                        loading='lazy'
                    />
                </Box>
                <Stack mt={6} direction={'row'} spacing={4} align={'center'}>
                    <Avatar src={'/siva.jpg'} />
                    <Stack direction={'column'} spacing={0} fontSize={'sm'}>
                        <Text fontWeight={600}>SIVA</Text>
                        <Text color={'gray.500'}>{date}</Text>
                    </Stack>
                </Stack>
            </Box>
        </Center>
    )
}

CardNews.propTypes = {
    src: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
}