import {Navbar, NavbarBrand, NavbarContent, NavbarItem, Link, Button} from "@nextui-org/react";

const NavBar = () => {
    return (
        <Navbar shouldHideOnScroll >
            <NavbarBrand>
                <p className="font-bold text-xl text-inherit">Puerto Quetzal</p>
            </NavbarBrand>
            <NavbarContent className="hidden sm:flex gap-4" justify="center">
                <NavbarItem>
                    <Link color="foreground" className="font-bold" href="/">
                        Inicio
                    </Link>
                </NavbarItem>
            </NavbarContent>
            <NavbarContent justify="end">
                <NavbarItem>
                    <Button color="primary" onClick={()=>{}} variant="flat">
                        Facturación
                    </Button>
                </NavbarItem>
            </NavbarContent>
        </Navbar>
    )
}

export default NavBar