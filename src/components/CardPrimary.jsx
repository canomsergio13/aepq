import {
  Box,
  Center,
  useColorModeValue,
  Stack,
  Image,
} from '@chakra-ui/react'
import Weather from './Weather'

const IMAGE ='/siva.jpg'

export default function CardPrimary() {
  return (
      <Center py={6}>
          <Box
            className='shadow-md '
            role={'group'}
            p={6}
            maxW={'330px'}
            w={'full'}
            bg={useColorModeValue('white', 'gray.800')}
            boxShadow={'2xl'}
            rounded={'lg'}
            pos={'relative'}
            zIndex={1}>
                <Box
                  rounded={'lg'}
                  mt={-12}
                  pos={'relative'}
                  height={'200px'}
                  _after={{
                    transition: 'all .3s ease',
                    content: '""',
                    w: 'full',
                    h: 'full',
                    pos: 'absolute',
                    top: 5,
                    left: 0,
                    backgroundImage: `url(${IMAGE})`,
                    filter: 'blur(15px)',
                    zIndex: -1,
                  }}
                  _groupHover={{ _after: { filter: 'blur(20px)' }}}>
                    <Image
                      rounded={'lg'}
                      height={200}
                      width={282}
                      objectFit={'cover'}
                      src={IMAGE}
                      alt="#"
                    />
                </Box>
                <Stack pt={10} align={'center'}>
                    <Weather />
                </Stack>
          </Box>
      </Center>
  )
}