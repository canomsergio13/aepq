import {
    Box,
    Center,
    useColorModeValue,
} from '@chakra-ui/react'
import PropTypes from 'prop-types'
  
export default function MyCard({children}) {
      return (
          <Center py={6}>
              <Box
                  maxW='400px'
                  w='full'
                  bg={useColorModeValue('white', 'gray.900')}
                  boxShadow='2xl'
                  rounded='md'
                  overflow='hidden'
                  p={6}
              >
                {children}
              </Box>
          </Center>
      )
}
  
MyCard.propTypes = {
    children: PropTypes.node.isRequired
}