import {
    Heading,
    Text,
    Stack,
    Image,
  } from '@chakra-ui/react'
import { useEffect, useState } from 'react';

const Weather = () => {
    const [weather, setWeather] = useState(null);

    useEffect(() => { 
        ( async () =>{
            const cities = ['Iztapa Guatemala'];
            const promises = [];

            for (const city of cities) {
                const promise = fetch(`https://api.weatherapi.com/v1/current.json?key=e648ba191b6b48ab875162613221312&q=${encodeURI(city)}&aqi=no&lang=es`);
                promises.push(promise);
            }

            try {
                const responses = await Promise.all(promises);
                const [data] = await Promise.all(responses.map(response => response.json()));
                setWeather(data);
            } catch (error) {
                console.error(error);
            }
        })()
    }, [])

    return (
        <>
            <Image
                objectFit={'cover'}
                height={90} width={90}   
                src={weather?.current?.condition?.icon}
                alt={weather?.current?.condition?.text}
            />
            <Heading fontSize='2xl' fontFamily='body' fontWeight={500}>
                Temperatura {weather?.current?.temp_c}°
            </Heading>
            <Heading fontSize='1xl' fontFamily='body' fontWeight={500}>
                {weather?.current?.condition?.text}
            </Heading>
            <Stack direction='row' align='center'>
                <Text fontWeight={800} fontSize='xl'>
                    Puerto Quetzal, Guatemala
                </Text>
            </Stack>
        </>
    )
}

export default Weather